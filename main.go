package main

import (
	"bufio"
	"bytes"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var inhibitCmd *exec.Cmd

type inhibitOut struct {
	Level string
	Text  string
}

func wakey() *exec.Cmd {
	log.Info().Msg("Starting systemd-inhibit")
	cmd := exec.Command(
		"systemd-inhibit",
		"--what=sleep",
		"--who=SSH Active User",
		"--why=Prevent sleep while SSH active",
		"--mode=block",
		"ping",
		"-i 60",
		"127.0.0.1",
	)

	//
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		panic(err)
	}

	if err := cmd.Start(); err != nil {
		panic(err)
	}

	readPipe := func(pipe *bufio.Reader, ch chan<- inhibitOut, wg *sync.WaitGroup, level string) {
		defer wg.Done()
		for {
			line, _, err := pipe.ReadLine()
			if err != nil {
				break
			}
			ch <- inhibitOut{Level: level, Text: string(line)}
		}
	}

	outCh := make(chan inhibitOut)
	var wg sync.WaitGroup

	wg.Add(2)
	go readPipe(bufio.NewReader(stdout), outCh, &wg, "info")
	go readPipe(bufio.NewReader(stderr), outCh, &wg, "error")

	go func() {
		wg.Wait()
		close(outCh)
		log.Info().Msg("systemd-inhibit process exited")
	}()

	go func() {
		for out := range outCh {
			if out.Level == "error" {
				log.Error().Msg(out.Text)
			} else {
				log.Info().Msg(out.Text)
			}
		}
	}()

	return cmd
}

func sleepy(cmd *exec.Cmd) {
	// kill systemd-inhibit
	err := cmd.Process.Kill()
	if err != nil {
		panic(err)
	}
}

func waity() {
	duration := 5 * time.Minute
	log.Info().Dur("duration", duration).Msg("wakey waity")
	time.Sleep(duration)
}

func hasActiveSSHSessions() bool {
	cmd := exec.Command("w")
	out, err := cmd.Output()
	if err != nil {
		panic(err)
	}
	scanner := bufio.NewScanner(bytes.NewReader(out))
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "sshd:") {
			return true
		}
	}
	return false
}

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	log.Info().Msg("wakey wakey")

	for {
		if hasActiveSSHSessions() {
			if inhibitCmd == nil {
				log.Info().Msg("SSH active. Entering inhibit mode")
				inhibitCmd = wakey()
			} else {
				log.Info().Msg("SSH active. Still in inhibit mode")
			}
		} else {
			if inhibitCmd != nil {
				log.Info().Msg("SSH no longer active. Exiting inhibit mode")
				sleepy(inhibitCmd)
				inhibitCmd = nil
			}
		}
		waity()
	}
}
