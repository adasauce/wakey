# Wakey Wakey!

🚀 This lil' buddy is all about keeping your server lit 🔥 when it's party time (aka when there's an active SSH sesh going on). 
Ain't nobody got time for the server to hit the hay 💤 when you're in the zone, coding away or just vibing in the terminal.

Here's the lowdown: we're peeping 👀 the server to see who's hanging out in SSH land and how long they've been ghosting their terminal. 
Like, if everyone's been AFK long enough to binge-watch a season of "Stranger Things," the code's like "aight, peace out" and lets the server snooze 😴. 

But, if there's even one soul out there keeping the dream alive, we're like "nah, let's keep the party going 🎉."

So, just boot up this program, and it'll vibe check your server's SSH sessions like a boss. 

It's kinda like having a chill guardian angel 👼 for your server, making sure it parties when it should and rests when it's time.

This is all about balance, fam. Keeping the vibes right ✌️  while making sure your server and electricity bill aren't throwing a fit.


## Usage

```
wakey
```

## Notes

This doesn't disconnect idle or dead SSH sessions.

If you want to be able to close your laptop and then let your workstation go to sleep on its own, make sure you configure sshd clean dead sessions up.

edit ClientAliveInterval and ClientAliveCountMax in sshd_config to your liking.
